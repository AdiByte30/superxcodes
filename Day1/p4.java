import java.io.*;

class Day1 {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br  = new BufferedReader (new InputStreamReader (System.in));
		

		System.out.println("Enter start :  ");
		int start = Integer.parseInt(br.readLine());
		System.out.println("Enter end   :  ");
		int end = Integer.parseInt(br.readLine());

		for(int i=start; i<=end ; i++ ) {
		
			if(i%2==1)
				System.out.println(i);
		

		}


	}
}
