import java.io.*;

class Day1 {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br  = new BufferedReader (new InputStreamReader (System.in));

		System.out.println("Enter rows : ");
		int rows = Integer.parseInt(br.readLine());
		int t = 0;	
		for(int i=1; i<=rows; i++ ) {
		
			for(int j=1; j<=i; j++) {
				
				t++;
					
				System.out.print( t+ "\t");
			}
			t--;
			System.out.println();
			

		}


	}
}
