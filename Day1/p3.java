// even odd

import java.io.*;

class Day1 {


	static void evenodd(int num) {
	

		if(num%2==0 && num!=0)
			System.out.println(num + " is even ");
		else if(num==0)
			System.out.println(num + " is Zero ");
		else 
			System.out.println(num + " is odd ");
	}


	public static void main(String[] args) throws IOException {
	
		BufferedReader br  = new BufferedReader (new InputStreamReader (System.in));

		int num = Integer.parseInt(br.readLine());


		evenodd(num);
		

	}
}
