
import java.io.*;

class Day2 {


	static void func(String str) {
	
		int count = 0;

		char arr[] = str.toCharArray();

		for(int i=0; i<arr.length; i++) {
		
			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' ||  arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' ||arr[i]=='O' || arr[i]=='U')
			       count++;	
		}
	
		System.out.println(count);

	}

	public static void main(String[] args) throws IOException {
	
		BufferedReader br  = new BufferedReader (new InputStreamReader (System.in));

		System.out.println("Enter String : ");
		String str =br.readLine();

		func(str);
			

	


	}
}
