// prime

import java.io.*;

class Day2 {

	static void func(int num) {

		int flag=0;
		for(int i=2; i<=num/2; i++) {
		
			if(num%i==0){			
				flag=1;
				break;
			}
		}

		if(num==2 || flag==0) 
			System.out.println(num+" is prime number");
		else
			System.out.println(num+" is composite number");
	
	}
	public static void main(String[] args) throws IOException {
	
		BufferedReader br  = new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter num : ");
		int num = Integer.parseInt(br.readLine());

		func(num);
			
			

	}
}
